#!/bin/bash

for d in /sys/kernel/iommu_groups/*/devices/*; do
  n=${d#*/iommu_groups/*}; n=${n%%/*}
  #printf 'IOMMU Group %s ' "$n"
  echo -n "${n} "
  lspci -nns "${d##*/}"
done \
  | sort --numeric-sort --key=1 \
  | awk -F' ' '
      { group=$1; $1=""; out[group]=out[group]" "$0"\n"; }

      END {
        for (group in out) {
          print "IOMMU Group "group":";
          print out[group];
        }
      }
  '
