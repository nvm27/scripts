#!/usr/bin/env bash

EFI_MOUNT_PATH="/boot/efi"
EFI_KERNEL_DIR="/boot/efi/EFI/Gentoo"
EFI_REFIND_PATH="/boot/efi/EFI/Boot/bootx64.efi"

SRC_REFIND_PATH="/usr/lib64/refind/refind/refind_x64.efi"
DEFAULT_CONFIG="/proc/config.gz"

EFIBOOTMGR_COMMAND=(efibootmgr --create --disk /dev/nvme0n1 --part 1 --loader /EFI/Boot/bootx64.efi --label "rEFInd Boot Manager" --verbose)

set -e

green="$(tput setaf 2)"
red="$(tput setaf 9)"
bold="$(tput bold)"
reset="$(tput sgr0)"

function info() {
  echo "${green}*${reset} $1"
}

function error() {
  echo "${red}*${reset} $1"
  exit 1
}

function ask_yn() {
  echo -n "${green}*${reset} "
  read -p "$1 " -n 1 -r
  echo

  [[ ${REPLY} =~ ^[Yy]$ ]] && return 0
  [[ ${REPLY} =~ ^[Nn]$ ]] && return 1

  error "Unexpected key pressed. Stopping..."
}

function umount_efi() {
  echo
  info "Umounting ${EFI_MOUNT_PATH} ..."
  umount "${EFI_MOUNT_PATH}"
}

function ensure_efi_partition() {
  if [[ ! -d "${EFI_KERNEL_DIR}" ]]; then
    echo

    info "Directory ${bold}${EFI_KERNEL_DIR}${reset} not available. Mounting ${bold}${EFI_MOUNT_PATH}${reset} ..."
    if mount "${EFI_MOUNT_PATH}"; then
      trap umount_efi EXIT
    else
      error "Unable to mount ${EFI_MOUNT_PATH}. Stopping..."
    fi
  fi
}

info "Currently running kernel version: ${bold}$(uname -r)${reset}"

last_kernel="$(basename "$(readlink -f /boot/kernel)" | sed 's/^vmlinuz-//')"
info "Last compiled kernel version: ${bold}${last_kernel}${reset}"

echo
selected_kernel="$(basename "$(readlink -f /usr/src/linux)" | sed 's/^linux-//')"
info "Kernel selected for compilation: ${bold}${selected_kernel}${reset}"

if ask_yn "Do you want to proceed with kernel compilation (y/n)?"; then
  echo
  genkernel_command=""

  if [[ -r "/usr/src/linux/.config" ]]; then
    info "Using already available kernel configuration: ${bold}/usr/src/linux/.config${reset}"
    genkernel_command="genkernel --nconfig all"
  else
    info "No kernel configuraion found in sources directoryu."
    info "Using currently running kernel configuration: ${bold}${DEFAULT_CONFIG}${reset}"
    genkernel_command="genkernel --kernel-config=${DEFAULT_CONFIG} --nconfig all"
  fi

  if ask_yn "Do you want to proceed with: ${bold}${genkernel_command}${reset} (y/n)?"; then
    echo
    ${genkernel_command}
  fi
fi

echo
new_kernel="$(readlink -f /boot/kernel)"
new_initramfs="$(readlink -f /boot/initramfs)"

info "Last compiled kernel version: ${bold}${new_kernel}${reset}"
info "Last created initramfs version: ${bold}${new_initramfs}${reset}"

if ask_yn "Do you want to install them on EFI parition (y/n)?"; then
  ensure_efi_partition

  echo
  echo -n "${green}*${reset} Copying "
  cp -v "${new_kernel}" "${EFI_KERNEL_DIR}"
  echo -n "${green}*${reset} Copying "
  cp -v "${new_initramfs}" "${EFI_KERNEL_DIR}"
fi

echo
if ask_yn "Do you want to copy rEFInd to EFI parition (y/n)?"; then
  ensure_efi_partition

  echo -n "${green}*${reset} Copying "
  cp -v "${SRC_REFIND_PATH}" "${EFI_REFIND_PATH}"
fi

if ! efibootmgr | grep "rEFInd Boot Manager" >/dev/null; then
  echo

  info "rEFInd is not explicitly added to EFI boot manager."
  info "Install command: ${bold}${EFIBOOTMGR_COMMAND[*]}${reset}"
  if ask_yn "Do you want to execute the command above (y/n)?"; then
    "${EFIBOOTMGR_COMMAND[@]}"
  fi
fi
