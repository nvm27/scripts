#!/usr/bin/env bash

HOST_CPUS="6-11,18-23"
#HOST_CPUS="0-5,12-17"
ALL_CPUS="0-23"

enable_isolation () {
  echo "Assigning host processes to CPU: ${HOST_CPUS}"
  systemctl set-property --runtime -- user.slice AllowedCPUs="${HOST_CPUS}"
  systemctl set-property --runtime -- system.slice AllowedCPUs="${HOST_CPUS}"
  systemctl set-property --runtime -- init.scope AllowedCPUs="${HOST_CPUS}"
}

disable_isolation () {
  echo "Assigning host processes to CPU: ${ALL_CPUS}"
  systemctl set-property --runtime -- user.slice AllowedCPUs="${ALL_CPUS}"
  systemctl set-property --runtime -- system.slice AllowedCPUs="${ALL_CPUS}"
  systemctl set-property --runtime -- init.scope AllowedCPUs="${ALL_CPUS}"
}


case "$2" in
  "prepare")
    enable_isolation
    ;;
  "release")
    disable_isolation
    ;;
  *)
    echo "Usage ${0} X prepare|release" >&2
    exit 1
esac
